use std::io::{self, Write};

pub mod pizza;

pub fn get_float(text: &str) -> f32 {
    loop {
        print!("{text}: ");
        io::stdout().flush().unwrap();
        let mut input = String::new();
        io::stdin().read_line(&mut input).expect("Failed to read line");
        match input.trim().parse::<f32>() {
            Ok(num) => return num,
            Err(_) => println!("Please enter a valid number!"),
        }
    }
}
