use std::cmp::Ordering;

use owo_colors::OwoColorize;

use pizza_calculator::get_float;
use pizza_calculator::pizza::{Pizza, PizzaSize};

fn create_pizza(size: PizzaSize) -> Pizza {
    let diameter: f32 = get_float(
        &format!("{} for {size}", "Diameter".magenta().bold())
    );
    let price: f32 = get_float(
        &format!("{} for {size}", "Price".green().bold())
    );
    println!("");
    Pizza::new(size, diameter, price)
}

fn main() {
    let small_pizza = create_pizza(PizzaSize::Small);
    let large_pizza = create_pizza(PizzaSize::Large);

    println!("
{small_pizza}
{large_pizza}

Scores:
    {}: {}
    {}: {}",
    small_pizza.size, small_pizza.score(),
    large_pizza.size, large_pizza.score());
    
    let winner: Option<PizzaSize> = match small_pizza.partial_cmp(&large_pizza).unwrap() {
        Ordering::Less => Some(large_pizza.size),
        Ordering::Greater => Some(small_pizza.size),
        Ordering::Equal => None,
    };

    match winner {
        Some(size) => println!("The Winner is: {size}"),
        None => println!("Your pizza chef is an OCD math professor. Both pizzas are equal!")
    };
}
