use std::fmt;
use std::cmp::{PartialEq, PartialOrd, Ordering};
use std::f32::consts::PI;

use owo_colors::OwoColorize;

pub enum PizzaSize {
    Small,
    Large,
}

impl fmt::Display for PizzaSize {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let text = match self {
            PizzaSize::Small => "Small Pizza".cyan().bold().to_string(),
            PizzaSize::Large => "Large Pizza".red().bold().to_string()
        };
        write!(f, "{text}")
    }
}

pub struct Pizza {
    pub size: PizzaSize,
    pub diameter: f32,
    pub price: f32,
}

impl Pizza {
    pub fn new(size: PizzaSize, diameter: f32, price: f32) -> Pizza {
        Pizza { size, diameter, price }
    }

    pub fn area(&self) -> f32 {
        let radius = self.diameter / 2.0;
        PI * radius.powi(2)
    }

    pub fn score(&self) -> f32 {
        self.area() / self.price
    }
}

impl PartialEq for Pizza {
    fn eq(&self, other: &Self) -> bool {
        self.score() == other.score()
    }
}

impl PartialOrd for Pizza {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        self.score().partial_cmp(&other.score())
    }
}

impl fmt::Display for Pizza {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let diameter_text = format!("{}cm {}", self.diameter, "diameter".magenta());
        let price_text = format!("{}₺ {}", self.price, "price".green());
        write!(f,"{} is a pizza with {} and {}.", self.size, diameter_text.bold(), price_text.bold())
    }
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn small_is_effective() {
        let small_pizza = Pizza::new(PizzaSize::Small, 26.0, 54.9);
        let large_pizza = Pizza::new(PizzaSize::Large, 32.0, 109.9);

        assert!(small_pizza > large_pizza);
    }

    #[test]
    fn large_is_effective() {
        let small_pizza = Pizza::new(PizzaSize::Small, 26.0, 69.0);
        let large_pizza = Pizza::new(PizzaSize::Large, 32.0, 99.9);

        assert!(large_pizza > small_pizza);
    }

    #[test]
    fn both_equal() {
        let small_pizza = Pizza::new(PizzaSize::Small, 26.702324, 70.0);
        let large_pizza = Pizza::new(PizzaSize::Large, 31.915382, 100.0);
        println!("{}", small_pizza.score());
        println!("{}", large_pizza.score());

        assert!(large_pizza == small_pizza);
    }
}
