# PIZZA CALCULATOR

## What is this?
Just a simple project that I wrote as a Rust practice. And also I couldn't decide which pizza to
order for my lunch break...

## Why Rust?
Because it's Rust...

## Why this code is so basic?
1. I'm new to Rust
2. It's just a simple circle area calculator... What else should I do?
